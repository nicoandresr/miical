import styled from '@emotion/styled';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { Button } from '@miical/ui';
import Typography from '@mui/material/Typography';
import { Day } from '@miical/ui';
import { Week } from '@miical/ui';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
  typography: {
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
});

const Main = styled.main`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-top: 2rem;
`;

const mondaySubjets = [
	{ start: '10:00', end: '10:45', name: 'hi' },
	{ start: '10:45', end: '11:45', name: 'ku' },
	{ start: '12:15', end: '13:00', name: 'ku' },
	{ start: '13:00', end: '13:45', name: 'et' },
	{ start: '13:45', end: '14:30', name: 'ma/b' },
];

export function App() {
  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <Main>
				<Typography variant="h1">MiiCale</Typography>
				<Week>
					<Day name="Monday" subjects={mondaySubjets} />
				</Week>
			</Main>
    </ThemeProvider>
  );
}

export default App;
