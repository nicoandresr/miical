import { render } from '@testing-library/react';

import Day from './Day';

describe('Day', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Day />);
    expect(baseElement).toBeTruthy();
  });
});
