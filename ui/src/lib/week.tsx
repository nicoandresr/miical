import * as React from 'react';
import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface WeekProps {}

const Root = styled.div`
	display: flex;
  gap: 1rem;
	border: 1px solid ${props => props.theme.palette.primary.main};
	padding: 1rem;
  margin-top: 2rem;
`;

export function Week(props: React.PropsWithChildren<WeekProps>) {
  return (
    <Root>
			{props.children}
    </Root>
  );
}

export default Week;
