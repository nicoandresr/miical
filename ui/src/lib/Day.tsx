import styled from '@emotion/styled';
import Typography from '@mui/material/Typography';

interface DayProps {
	name: string;
	subjects: Array<{
		name: string;
		start: string;
		end: string;
	}>
}

const DayRoot = styled.div`
 display: flex;
 align-items: center;
 justify-content: space-between;
 flex-direction: column;
 border: 1px solid ${props => props.theme.palette.primary.main};
 padding: 1rem;
`;

const DescriptionList = styled.dl`
	display: flex;
	flex-direction: column;
  flex: 1;
`;

const DescriptionTerm = styled.dt`
`;

const DescriptionDescription = styled.dd`
`;

const Row = styled.div`
	display: flex;
`;

export function Day(props: DayProps) {
  return (
	  <DayRoot>
			<Typography variant="h2">{props.name}</Typography>

			<DescriptionList>
				{props.subjects.map((subject) => (
					<Row key={subject.name}>
						<DescriptionTerm>{subject.start} - {subject.end}</DescriptionTerm>
						<DescriptionDescription>{subject.name}</DescriptionDescription>
					</Row>
				))}
			</DescriptionList>
		</DayRoot>
	);
}

export default Day;
